<?php


use Phinx\Migration\AbstractMigration;

class Quotes extends AbstractMigration
{

    public function up(){
        // Quotes
        $table = $this->table('quotes');
        $table
            ->addColumn('chat_id', 'biginteger')
            ->addColumn('quotee',  'text')
            ->addColumn('quoter',  'text')
            ->addColumn('text',    'text')
            ->addTimestamps()
            ->create();
    }

    public function down(){
        $this->table('quotes')->drop();
    }
}

<?php


use Phinx\Migration\AbstractMigration;

class TwentyXxMigration extends AbstractMigration
{

    public function up(){
        // Last.FM users table
        $table = $this->table('lastfm');
        $table
            ->addColumn('user_id',  'biginteger')
            ->addColumn('username', 'text')
            ->addTimestamps()
            ->create();
        
        // Term counters
        $table = $this->table('term-count');
        $table
            ->addColumn('chat_id', 'biginteger')
            ->addColumn('term',    'text')
            ->addColumn('count',   'biginteger')
            ->addTimestamps()
            ->create();
    }

    public function down(){
        $this->table('term-count')->drop();
        $this->table('lastfm')->drop();
    }
}

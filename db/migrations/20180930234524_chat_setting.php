<?php

use Phinx\Migration\AbstractMigration;

/**
 * Chat settings table for Litegram
 */
class ChatSetting extends AbstractMigration
{

    public function up(){
        $table = $this->table('chat-settings');
        $table
            ->addColumn('chat_id', 'biginteger')
            ->addColumn('nsfw', 'boolean')
            ->addTimestamps()
            ->create();
    }

    public function down(){
        $this->table('chat-settings')->drop();
    }
}

*Usage*: /{{name}} dice-count=1 dice-sides=20

    Rolls dice.

The maximum value allowed for both is 256.
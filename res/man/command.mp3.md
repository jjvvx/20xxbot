*Usage*: /{{name}} url

    Downloads music from YouTube and other sites.

Compatible sites: https://rg3.github.io/youtube-dl/supportedsites.html
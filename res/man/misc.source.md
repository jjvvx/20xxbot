TwentyXXBot's code is available on [GitLab](https://gitlab.com/0x486/20xxbot)
and licensed under the BSD 3-Clause license.

This project is powered by [Litegram](https://gitlab.com/litegram/skeleton)
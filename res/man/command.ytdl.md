*Usage*: /{{name}} url

    Downloads a video from YouTube and other sites.

Compatible websites: https://rg3.github.io/youtube-dl/supportedsites.html
# 20XXBot

## Requirements

- PHP 7.1 or newer
- Composer
- A database compatible with Eloquent

## Installation and configuration

1. Follow the instructions in [litegram](https://gitlab.com/litegram/skeleton)

2. Migrate the database
    ```bash
    php vendor/bin/phinx migrate
    ```

3. Add the needed API key for Last.FM
    ```bash
    ./litegram apikeys:new lastfm YOUR_LASTFM_KEY
    ```
   Or alternatively, disable the LastFm module in `app/config.php`


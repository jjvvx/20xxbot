<?php

namespace Litegram;

use Psr\Container\ContainerInterface;

use function DI\env;
use function DI\add;
use App\Http\Controllers\TestController;
use App\Telegram\Modules\Ytdl;
use App\Telegram\Modules\Random;
use App\Telegram\Modules\Source;
use App\Telegram\Modules\Gelbooru;
use App\Telegram\Modules\LastFm;
use App\Telegram\Modules\TermCount;
use App\Telegram\Modules\Emote;
use App\Telegram\Modules\Spurdo;
use App\Telegram\Modules\Quotes;
use App\Http\Controllers\QuotesController;
use App\Http\Controllers\Home;
use App\Http\HtmlErrorHandler;
use Monolog\Handler\NativeMailerHandler;
use Monolog\Logger;

return [

    // youtube-dl

    'ytdl.binpath' => env('YTDL_BIN'),

    // Error logging
    //

    'log.email.to'      => env('LOG_EMAIL_TO'),
    'log.email.from'    => env('LOG_EMAIL_FROM'),
    'log.email.subject' => env('LOG_EMAIL_SUBJECT', '20XX Error'),

    'monolog.name'       => '20XXBot',
    'monolog.handlers'   => \DI\add([
        function(ContainerInterface $c){
            $to      = $c->get('log.email.to');
            $subject = $c->get('log.email.subject');
            $from    = $c->get('log.email.from');

            return new NativeMailerHandler($to, $subject, $from, Logger::ERROR);
        }
    ]),
    'monolog.processors' => \DI\add([
        //
    ]),

    // Eloquent
    //

    'db' => [
        'driver'    => env('DB_DRIVER'),
        'host'      => env('DB_HOST'),
        'database'  => env('DB_NAME'),
        'username'  => env('DB_USER'),
        'password'  => env('DB_PASS'),
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
    ],

    // Telegram
    //

    'telegram.modules' => add([
        Source::class,
        //Ytdl::class, // Disabled; too buggy
        Random::class,
        LastFm::class,
        TermCount::class,
        Emote::class,
        Spurdo::class,
        Quotes::class,
    ]),

    'telegram.token' => env('TG_TOKEN'),

    // Http
    //

    'http.html-error-handler' => \DI\get(HtmlErrorHandler::class),

    'http.controllers' => add([
        Home::class
    ]),

    // Command line
    //

    'cli.commands' => add([
        //
    ]),

];
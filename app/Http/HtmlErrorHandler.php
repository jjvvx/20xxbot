<?php

namespace App\Http;

use Throwable;

/**
 * HTML renderer for error handling
 */
class HtmlErrorHandler{

    public function __invoke(Throwable $t, bool $display){
        return '';
    }
}
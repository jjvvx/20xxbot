<?php

namespace App\Http\Controllers;

use Litegram\Http\Controllers\Controller;
use Litegram\App;
use Slim\Views\Twig;
use Litegram\Telegram\Api;

class Home implements Controller{

    public function addRoutes(App $app){
        $app->get('/', $this);
    }

    public function __invoke($response, Twig $twig, Api $api){
        $me = $api->getMe();
        return $twig->render($response, 'home.twig', ['me' => $me]);
    }
}
<?php

namespace App\Telegram\Modules;

use Litegram\Telegram\Module;
use Litegram\Telegram\Router;
use Litegram\Telegram\Result\Message;
use Litegram\Helpers\Markdown;

class Emote implements Module{

    const EMOTES = [
        'love'      => '(✿  ・‿-) ~ ♡',
        'sadwave'   => '(´・~・`)ノ',
        'awkward'   => '( ；・ω・)',
        'wee'       => 'ミ(　 ・ω・)',
        'denko'     => '(´・ω・`)',
        'shrug'     => '¯\_(ツ)_/¯',
        'shrug2'    => '┐(\'~\'; )┌',
        'yay'       => 'ヽ(•ヮ• ヽ)',
        'wink'      => '( ´・‿-)',
        'ohboy'     => 'ᕕ( ᐛ )ᕗ',
        'what'      => '( ´_ゝ`)',
        'mad'       => '(⇀‸↼‶)',
        'lenny'     => '( ͡° ͜ʖ ͡°)',
        'hug'       => '(っ･ω･)っ',
        'evil'      => 'ψ(｀∇´)ψ',
        'worried'   => '(´･_･`)',
        'baka'      => '(*´>д<)',
        'flip'      => '(╯°□°)╯︵ ┻━┻',
        'putback'   => '┬─┬ ノ( ゜-゜ノ)',
        'chillsmug' => '¦¬)'
    ];


    public function addRoutes(Router $r){
        $r->addCommand('emote [name]', [$this, 'emote']);
    }

    public function emote(Message $m, string $name = null){
        if(!$name){
            $out = Markdown::bold("Available emotes")."\n";

            foreach(self::EMOTES as $n => $emote)
                $out.= Markdown::bold($n).': '.Markdown::escape($emote)."\n";

            return Markdown::setMode($m->reply($out));
        }
        return $m->reply(self::EMOTES[$name] ?? 'Emote not found');
    }
}
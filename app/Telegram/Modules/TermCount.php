<?php

namespace App\Telegram\Modules;

use Litegram\Telegram\Module;
use Litegram\Telegram\Router;
use Litegram\Telegram\Result\Message;
use App\Database\Models\Terms;
use Litegram\Except\Id10tException;
use Litegram\Helpers\Markdown;

class TermCount implements Module{

    protected $text;

    public function addRoutes(Router $r){
        // 'terms [sub] [term]'
        $r->addCommand('terms [sub] [term]', [$this, 'command']);
        //
        $r->addTextEvent('termcount', '/./', [$this, 'check']);
    }

    public function check(Message $m){
        $chatId = $m->getChat()->getId();
        $words  = array_map('strtolower', $m->getWords());

        $terms = Terms::where('chat_id', $chatId)->get();

        $got = [];
        foreach($terms as $term){
            if(in_array($term->term, $words)){
                $count = $this->update($chatId, $term->term);
                if($this->gotEm($term->term, $count))
                    $got[$term->term] = $count;
            }
        }
    
        if(!empty($got)){
            $out = '';
            foreach($got as $term => $count){
                $out.= Markdown::bold($term).' count: '.$count."\n";
            }
            
            return Markdown::setMode($m->reply($out));
        }
    }

    public function gotEm(string $term, int $count): bool{
        $arr = str_split($count);
        $arr = array_reverse($arr);
        $same = true;
        for($i = 0; $same && isset($arr[$i]); $i++){
            $current = $arr[$i];
            $next = $arr[$i+1] ?? null;
            $same = ($next == $current);
        }
        return ($i > 1);
    }

    public function command(Message $m, string $sub = null, string $term = null){
        $chatId = $m->getChat()->getId();

        switch($sub){
            case 'add':
                $this->addTerm($chatId, $term);
                return $m->reply('Term added');
            break;

            case 'rm':
                $this->rmTerm($chatId, $term);
                return $m->reply('Removed');
            break;

            case 'list':
            case null:
                $terms = $this->getAllTerms($chatId);
                $out = "Terms:\n";
                foreach($terms as $term => $count){
                    $out.= Markdown::bold($term).': '.
                        Markdown::pre($count)."\n";
                }
                return Markdown::setMode($m->reply($out));
            break;
            
            default:
                return $m->reply("Unknown option $sub");
            break;
        }
    }

    public function getTermCount(int $chatId, string $term): int{
        $terms = Terms::where('chat_id', $chatId)
            ->where('term', $term);
        
        if($terms->count() == 0)
            throw new Id10tException("Term not found");
        
        return $terms->first()->count;
    }

    /**
     * @return int[]
     */
    public function getAllTerms(int $chatId): array{
        $terms = Terms::where('chat_id', $chatId)->get();

        $return = [];
        foreach($terms as $term){
            $return[$term->term] = $term->count;
        }

        return $return;
    }

    public function addTerm(int $chatId, string $newTerm){
        $term = new Terms();
        $term->chat_id = $chatId;
        $term->term    = strtolower($newTerm);
        $term->count   = 0;
        $term->save();
    }

    public function rmTerm(int $chatId, string $term){
        Terms::where('chat_id', $chatId)
            ->where('term', $term)->delete();
    }

    public function update(int $chatId, string $term){
        $count = $this->getTermCount($chatId, $term);
        Terms::where('chat_id', $chatId)
            ->where('term', $term)
            ->update([ 'count' => ++$count ]);

        return $count;
    }
}
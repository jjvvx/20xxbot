<?php

namespace App\Telegram\Modules;

use Litegram\Telegram\Module;
use Litegram\Telegram\Router;
use Litegram\Telegram\Query;
use Litegram\Telegram\Result\Message;
use YoutubeDl\Exception\YoutubeDlException;
use YoutubeDl\YoutubeDl;
use YoutubeDl\Entity\Video;
use Exception;
use Litegram\Except\Id10tException;
use Litegram\Telegram\Api;
use Litegram\Telegram\InFile;
use Litegram\Database\Cache;
use Psr\Container\ContainerInterface;
use Litegram\Telegram\Result\Error;
use Litegram\Telegram\Result\Audio;
use Litegram\Telegram\Result\Video as TgVideo;
use Litegram\Telegram\Result\Document;

class Ytdl implements Module{

    const OPTIONS = [
        'max-filesize' => '50m',
    ];

    /** @var Cache */
    private $cache;

    /** @var Api */
    private $api;

    /** @var string */
    private $binPath;

    public function __construct(Api $api, Cache $cache, ContainerInterface $c){
        $this->api     = $api;
        $this->cache   = $cache;
        $this->binPath = $c->get('ytdl.binpath');
    }

    public function addRoutes(Router $r){
        $r->addCommand('ytdl url', [$this, 'ytdl']);
        $r->addCommand('mp3 url', [$this, 'mp3']);
    }

    public function ytdl(string $url, Message $m){
        if($fileId = $this->getCache($url, 'mp4')){
            $q = new Query('sendVideo');
            $q->setParameter('chat_id', $m->getChat()->getId());
            $q->setParameter('video', $fileId);
            $q->setParameter('caption', $url.' [cached]');
            return $q;
        }

        $this->verifyLink($url, 'mp4', $m);

        $result = $this->download([], $url);

        if(is_string($result))
            return $m->reply($result);
        
        $f  = $result->getFile()->getPathname();
        $fp = \GuzzleHttp\Psr7\stream_for(fopen($f, 'r'));
        $if = new InFile($fp);

        $q  = new Query('sendVideo', [
            'chat_id' => $m->getChat()->getId(),
            'video'   => $if,
            'caption' => $url
        ]);

        $ul = $this->api->query($q);

        if($ul instanceof Error)
            return $m->reply('Upload failed');
        
        if($video = $ul->getVideo())
            $this->setCache($url, 'mp4', $video->getFileId());

        unlink($f);
    }

    public function mp3(string $url, Message $m){
        if($fileId = $this->getCache($url, 'mp3')){
            $q = new Query('sendAudio');
            $q->setParameter('chat_id', $m->getChat()->getId());
            $q->setParameter('audio',   $fileId);
            $q->setParameter('caption', $url.' [cached]');
            return $q;
        }

        $this->verifyLink($url, 'mp3', $m);

        $result = $this->download([
            'extract-audio' => true,
            'audio-format'  => 'mp3'
        ], $url);

        if(is_string($result))
            return $m->reply($result);
        
        $f  = $result->getFile()->getPathname();
        $fp = \GuzzleHttp\Psr7\stream_for(fopen($f, 'r'));
        $if = new InFile($fp);

        $q  = new Query('sendAudio', [
            'chat_id' => $m->getChat()->getId(),
            'audio'   => $if,
            'caption' => $url,

            'title'  => $result->getTitle(),
            'artist' => $result->getUploader()
        ]);

        $ul = $this->api->query($q);

        if($ul instanceof Error)
            return $m->reply('Upload failed');
        
        $this->setCache($url, 'mp3', $ul->getAudio()->getFileId());

        unlink($f);
    }

    /**
     * Downloads a file or returns string on error.
     * 
     * @param array $opts
     * @param string $url
     * 
     * @return Video|string
     */
    public function download(array $opts, string $url){
        $ytdl = $this->getYtdl($opts);
        try{
            $dl = $ytdl->download($url);

            if(!file_exists($dl->getFile()->getPathname())){
                return 'Could not download: file too large';
            }

        }catch(NotFoundException $e) {
            return 'Could not download: file not found';
        } catch(PrivateVideoException $e) {
            return 'Could not download: private video';
        } catch(CopyrightException $e) {
            return 'Could not download: content is copyrighted';
        } catch(\Exception $e) {
            return 'Internal error: '. $e->getMessage();
        }

        return $dl;
    }

    public function verifyLink(string $url, string $format, Message $m){
        $ytdl = $this->getYtdl([
            'skip-download' => true
        ]);
        try{
            $vid = $ytdl->download($url);
        }catch(Exception $e){
            throw new Id10tException('Internal error');
        }

        // Video is age-restricted or from an adult video website.
        // Regular groups are the only place where Telegram's ToS doesn't care.
        if($vid->getAgeLimit() >= 18 && $m->getChat()->getType() != 'group'){
            throw
            new Id10tException('Could not download: age-restricted content');
        }
    }

    /**
     * Get a YoutubeDl instance
     * 
     * @param array $options Options to pass to YoutubeDl
     * 
     * @return YoutubeDl
     */
    public function getYtdl(array $options){
        $ytdl = new YoutubeDl(self::OPTIONS + $options);
        $ytdl->setBinPath($this->binPath);
        $ytdl->setDownloadPath('/tmp');
        return $ytdl;
    }

    /**
     * Look for Telegram file ID in cache or return null
     * 
     * @param string $url File URL
     * @param string $format File format to look for
     * 
     * @return string|null
     */
    public function getCache(string $url, string $format){
        return $this->cache->getCache($format.':ytdl:'.$url);
    }

    /**
     * Save Telegram file ID in cache
     * 
     * @param string $url File URL
     * @param string $format File format to save
     * @param string $fileId Telegram file ID
     * 
     * @return void
     */
    public function setCache(string $url, string $format, string $fileId){
        $this->cache->setCache(
            $format.':ytdl:'.$url,
            $fileId,
            new \DateTime('next year')
        );
    }
}
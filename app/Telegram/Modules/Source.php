<?php

namespace App\Telegram\Modules;

use Litegram\Telegram\Module;
use Litegram\Telegram\Router;
use Litegram\Telegram\Result\Message;
use Litegram\Manual;

class Source implements Module{

    protected $text;

    public function __construct(Manual $man){
        $this->text = $man->getPage('misc.source');
    }

    public function addRoutes(Router $r){
        $r->addCommand('source', [$this, 'source']);
    }

    public function source(Message $m){
        return $m->reply($this->text)
            ->setParameter('parse_mode', 'Markdown');
    }
}
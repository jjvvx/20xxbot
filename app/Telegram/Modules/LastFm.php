<?php

namespace App\Telegram\Modules;

use App\Database\Models\LastFM as Database;

use Litegram\Telegram\Module;
use Litegram\Telegram\Router;
use Litegram\Telegram\Result\Message;
use Litegram\Database\KeyManager;
use GuzzleHttp\Client;

use Litegram\Helpers\Markdown;

class LastFm implements Module{

    const API_URI = "http://ws.audioscrobbler.com/2.0";

    /** @var string */
    private $key;

    /** @var Client */
    private $client;

    public function __construct(KeyManager $km){
        $this->key    = $km->getKey('lastfm');
        $this->client = new Client([
            'http_errors' => false
        ]);
    }

    public function addRoutes(Router $r){
        $r->addCommand('lastfm [user]', [$this, 'lastFm']);
        $r->addCommand('np [user]', [$this, 'nowPlaying']);
    }

    public function lastFm(Message $m, string $user = null){
        if(!$user)
            $user = $this->getUser($m->getFrom()->getId());
        
        $response = $this->method('user.getTopArtists', [
            'user'   => $user,
            'limit'  => 5,
            'period' => '7day'
        ]);

        if(isset($response->error))
            return $m->reply('Error: '.$response->message);
    
        $mention = Markdown::link(
            'tg://user?id='.$m->getFrom()->getId(),
            $m->getFrom()->getFirstName()
        );
        $text = "Top artists for ".$mention.":\n";
        foreach($response->topartists->artist as $artist){
            $rank = $artist->{'@attr'}->rank;
            $name = $artist->name;
            $link = $artist->url;

            $text.= Markdown::bold("#$rank").' '.
                    Markdown::link($link, $name)."\n";
        }

        $q = $m->reply($text)->setParameter('disable_web_page_preview', true);

        return Markdown::setMode($q);
    }

    public function nowPlaying(Message $m, string $user = null){
        if(!$user){
            $user = $this->getUser($m->getFrom()->getId());
        }

        $response = $this->method('user.getRecentTracks', ['user'=>$user]);

        if(isset($response->error))
            return $m->reply('Error: '.$response->message);

        if(empty($response->recenttracks->track))
            return $m->reply($user.' has no scrobbles.');

        $this->setUser($m->getFrom()->getId(), $user);

        $track = $response->recenttracks->track[0];

        if(!isset($track->{'@attr'}->nowplaying))
            return $m->reply($user.' is not playing anything right now.');

        $artist = $track->artist;
        $title  = $track->name;
        $cover  = $track->image[2]->{'#text'} ?? null;

        $q = $m->reply('Null')->setParameter('disable_web_page_preview', 1);
        $text = '';
        if($cover){
            $text.= "[\u{200B}]($cover)";
            $q->setParameter('disable_web_page_preview', 0);
        }
        
        $text.= 
            Markdown::escape($artist->{'#text'}).
            " - ".
            Markdown::link($track->url, $title);

        $q->setParameter('text', $text);
        return Markdown::setMode($q);
    }

    public function getUser(int $userId){
        $users = Database::where('user_id', $userId);

        if($users->count() == 0)
            return null;
        
        return $users->first()->username;
    }

    public function setUser(int $userId, string $userName){
        if($this->getUser($userId))
            $this->updateUser($userId, $userName);
        $user = new Database();
        $user->user_id  = $userId;
        $user->username = $userName;
        $user->save();
    }

    private function updateUser(int $userId, string $userName){
        $user = Database::where(['user_id' => $userId])
            ->update(['username' => $userName]);
    }

    public function method(string $name, array $parameters){
        $queryArguments['method']  = $name;
        $queryArguments['api_key'] = $this->key;
        $queryArguments['format']  = 'json';
        $query = self::API_URI.'?'.http_build_query($queryArguments + $parameters);

        return json_decode($this->client->get($query)->getBody());
    }
}
<?php

namespace App\Telegram\Modules;

use Litegram\Telegram\Module;
use Litegram\Telegram\Router;
use Litegram\Telegram\Result\Message;

/**
 * RNG-based commands.
 */
class Random implements Module{

    /** @var int */
    const MAX_RAND = 100;

    /** @var string[] */
    const EIGHTBALL_QUOTES = [
        // Positive outcomes.
		'It is certain.',
		'It is decidedly so.',
		'Without a doubt.',
		'Yes, definately.',
		'You may rely on it.',
		'As I see it, yes.',
		'Most likely.',
		'Outlook good.',
		'Yes.',
		'Signs point to yes.',
		// Unsure outcomes.
		'Reply hazy; try again later.',
		'Ask again later.',
		'Better not tell you now.',
		'Cannot predict now.',
		'Concentrate and ask again.',
		// Negative outcomes.
		'Don\'t count on it.',
		'My reply is no.',
		'My sources say no.',
		'Outlook is not so good.',
		'Very doubtful.'
    ];

    public function addRoutes(Router $r){
        $r->addCommand('dice [amount] [sides]', [$this, 'dice']);

        $r->addCommand('coin', [$this, 'coin']);

        $r->addCommand('8ball', [$this, 'eightBall']);

        $r->addTextEvent('no u', '/no ?u/i', [$this, 'noNoU']);
    }

    public function dice(Message $m, int $amount = 1, int $sides = 20){
        if($amount <= 0 || $sides <= 0)
            return $m->reply('Values cannot be 0 or lower.');
        if($amount > self::MAX_RAND || $sides > self::MAX_RAND)
            return $m->reply('Values cannot be higher than '.self::MAX_RAND);

        $total = 0;
        $dice  = [];
        for($i = 0; $i < $amount; $i++){
            $die = rand(1, $sides);
            $total += $die;
            $dice[] = $die;
        }
        return $m->reply(
            "Total: $total\n[".
            implode(', ', $dice).
            "]"
        );
    }

    public function eightBall(Message $m){
        return $m->reply(
            self::EIGHTBALL_QUOTES[array_rand(self::EIGHTBALL_QUOTES)]
        );
    }

    public function coin(Message $m){
        return $m->reply(
            'The coin landed on '.
            (rand(0,1) ? 'heads' : 'tails')
        );
    }

    public function noNoU(Message $m){
        if(rand(1, self::MAX_RAND) == self::MAX_RAND)
            return $m->reply('no u');
    }
}
<?php

namespace App\Telegram\Modules;

use Litegram\Telegram\Module;
use Litegram\Telegram\Router;
use App\Database\Models\Quote;
use Litegram\Telegram\Result\User;
use Litegram\Telegram\Result\Message;
use Litegram\Helpers\Markdown;


class Quotes implements Module{

    public function addRoutes(Router $r){
        $r->addCommand('quote [id]', [$this, 'run']);
    }

    public function run(Message $m, int $id = null){
        if($r = $m->getReplyTo()){
            if(!($r->getType() & Message::TEXT)){
                return $m->reply('Cannot quote non-text messages');
            }

            $this->addQuote(
                $m->getChat()->getId(),
                $m->getFrom(),
                $r->getFrom(),
                $r->getText()
            );

            return $m->reply("Added");
        }

        $chatId = $m->getChat()->getId();

        if(!($count = $this->countQuotes($chatId)))
            return $m->reply('This chat has no quotes');
        
        if($id === null || $id < 1)
            $id = rand(1, $count);

        $quote = $this->getQuote($m->getChat()->getId(), $id);

        if(!$quote)
            return $m->reply('Quote not found');

        $quoter = new User(json_decode($quote->quoter));
        $quotee = new User(json_decode($quote->quotee));

        $out = Markdown::bold("Quote $id/$count")."\n";
        $out.= Markdown::italics($quote->text)."\n";
        $out.= '  -- '.$quotee->getFirstName().", ";
        $out.= $quote->created_at->format('d-m-Y')."\n";
        $out.= 'Added by '.$quoter->getFirstName();

        return Markdown::setMode($m->reply($out));
    }

    public function countQuotes(int $chatId): int{
        return Quote::where('chat_id', $chatId)->count();
    }

    public function getQuote(int $chatId, int $id = null){
        $quotes = Quote::where('chat_id', $chatId)
            ->orderBy('id')
            ->get()
            ->all();
        
        if($id === null)
            $id = array_rand($quotes) + 1;
        
        return $quotes[$id-1] ?? null;
    }

    public function addQuote(
    int $chatId, User $quoter, User $quotee, string $text){
        $quote = new Quote();
        $quote->chat_id = $chatId;
        $quote->quoter  = (string) $quoter;
        $quote->quotee  = (string) $quotee;
        $quote->text    = $text;
        $quote->save();
    }
}